from fastapi import FastAPI
from mongopart.books_route import router as books_router

app = FastAPI()
app.include_router(books_router)

@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Applications!"}

params = {
    "id":"606cb08e67efa7e7a9196d1f",
    "data":{
        "tahunterbit":"2016",
        "genre":"Non Fiction"
    }
}