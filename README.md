# mongopart

mongoPart adalah bagian dari aplikasi peminjaman buku myLibApp yang khusus memproses tabel books. Fungsionalitas yang dibangun di bagian ini adalah CRUD (Create, Read, Update, Delete). Read terdiri atas fungsi yang melihat semua data atau data tertentu dengan id tertentu. Adapun API yang digunakan adalah FastAPI dengan keamanan menggunakan JWT tanpa ODM.
